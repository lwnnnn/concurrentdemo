package com.qycloud.juc.block;

import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Main{
    public static void main(String[] args) throws InterruptedException {
        LinkedBlockingQueue<String> list=new LinkedBlockingQueue<String>(3);
        Client client=new Client(list);
        Thread thread=new Thread(client);
        thread.start();

        for(int i=0;i<5;i++){
            for(int j=0;j<3;j++){
                try {
                    String request=list.take();
                    System.out.printf("Main: request: %s at %s. Size: %d\n",request,new Date(),list.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            TimeUnit.MILLISECONDS.sleep(300);
        }

    }
}
