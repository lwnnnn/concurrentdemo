package com.qycloud.juc.noblock;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 非阻塞线程安全列表(非阻塞式并发列表)
 */
public class AddTask implements Runnable {

    private ConcurrentLinkedDeque<String> list;

    public AddTask(ConcurrentLinkedDeque<String> list ) {
      this.list=list;
    }

    public void run() {
        String name=Thread.currentThread().getName();
        for(int  i=0;i<1000;i++){
            list.add(name + "Elemet "+i);
        }

    }
}
