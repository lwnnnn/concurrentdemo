package com.qycloud.juc.priority;

/**
 * 使用按优先级排序的阻塞式线程安全列表
 * 按优先级排序
 */

public class Event implements Comparable<Event> {
    private int thread;

    private int priority;

    public int getThread() {
        return thread;
    }

    public int getPriority() {
        return priority;
    }

    public Event(int thread, int priority){
        this.priority=priority;
        this.thread=thread;

    }


    public int compareTo(Event e) {
        if(this.priority>e.getPriority()){
            return -1;
        }else if(this.priority<e.getPriority()){
            return 1;

        }else{
            return 0;
        }

    }
}
